﻿using System; 
using System.Text;
using System.Numerics;
public static class Decoder {

public static string Coder(string textToCode)
{

        string s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,? ";
        string result = "";
        ulong currentNumber;
        int k = 0;
        foreach (char w in textToCode) {
            currentNumber = (uint)s.IndexOf(w);
            System.Console.Write(currentNumber + " ");
            if (currentNumber < 0 || currentNumber >= (ulong)(s.Length))  result += w;
            else {
            for (int i = 0; i <=k; i++)
              currentNumber = currentNumber * 2 +1;  
            
            System.Console.Write(currentNumber + " ");
            if (currentNumber > 65) 
              currentNumber = (currentNumber%67);
            System.Console.WriteLine(currentNumber + " ");
            // if 
            result += s[(int)currentNumber];
            
            }
          k++;
        }  
        return result;
}

public static int CompairStrings(string s1, string s2)  {
  int i = 0;
  while (i < s1.Length && i < s2.Length && s1[i] == s2[i])  i++;
  
  if (i == s1.Length) i = -1;
  return i;

}

public static string Decode(string p_what)  
{
        string symbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,? ";
        string result = "";
        int currentNumber;
        System.Console.WriteLine(p_what);
        int k = 0;
        foreach (char w in p_what) {
            currentNumber = symbols.IndexOf(w);
            if (currentNumber < 0)  result += w;
            else {
            for (int i = 0; i <=k; i++) {
              if (currentNumber ==0 || currentNumber%2 ==0)  currentNumber += 67;
              currentNumber = (currentNumber - 1) / 2;  
            }
            
            if (currentNumber > 65) 
              currentNumber = (currentNumber%67);
            result += symbols[currentNumber];
            
            }
          k++;
        } 
  return result;
}

  public static void Main() {
    string s1 = Coder("mid-century. The Venona intercepts contained");
    System.Console.WriteLine(s1);
    string s2 = "zJF-CZXBFglEWVNXUR?CQWg0YUCVxhW6UCdQBu7fOaMW";
    System.Console.WriteLine(s2);
    int j = CompairStrings(s1, s2);
        if (j < 0)  System.Console.WriteLine("ok");
        else  {
          System.Console.WriteLine("Number: {0}\t {1} in example\t {2} in result", j, s2[j], s1[j]);
        }
  }
}